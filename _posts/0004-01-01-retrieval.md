## Retrieval & Data filtering

Notes:
- following slides: overview of the critical tasks
- Explain common issues and pitfalls during data retrieval  

---

<span class="hoverElement" data-customTooltip="Application Programming Interface">API</span>s

- Flickr (flickr.photos.search) <a href="https://www.flickr.com/services/api/flickr.photos.search.html"> <sup class="smallsuper fa fa-external-link"></sup></a>
- Instagram (GraphQL) <a href="https://developers.facebook.com/docs/instagram-api/"> <sup class="smallsuper fa fa-external-link"></sup></a>
- Twitter (streaming API) <a href="https://developer.twitter.com/en/docs/tutorials/consuming-streaming-data"> <sup class="smallsuper fa fa-external-link"></sup></a>

Notes:
- Web APIs comparable, with custom syntax each
- e.g. Twitter: Mostly streaming based (listening necessary)

---

Example: <span class="highlight">Instagram API</span>

```python
hashtag = "park"
query_url = f'https://www.instagram.com/explore/tags/{hashtag}/?__a=1&__d=dis'
```

<ul>
<li class="fragment" data-fragment-index="1"><code class="highlight">?__a=1&__d=dis</code>: return as json</li>
<li class="fragment" data-fragment-index="2"><code class="highlight">f'{var}'</code>: f-strings <sup><a href="https://docs.python.org/3/tutorial/inputoutput.html#formatted-string-literals" class="smallsuper"><i data-customTooltip="> Python 3.6: Formatted String Literals." class="fa fa-external-link"></i></sup></a></li>
<li class="fragment" data-fragment-index="3">Live Query to the API <sup><a href="https://www.instagram.com/explore/tags/park/?__a=1&__d=dis" class="smallsuper"><i data-customTooltip="Works with & without login." class="fa fa-external-link"></i></sup></a></li>
</ul>

Notes:
- Instagram API sample
- Web API, GraphQL, discontinued but still publiy (why?)
- Consumer views endpoint as a url (path)
- API processes requests based on the path/endpoint

---

<pre class="python"><code data-trim data-noescape data-line-numbers="1-10|4|4-6|4-10" data-fragment-index="1">
hashtag = "park"
query_url = f'https://www.instagram.com/explore/tags/{hashtag}/?__a=1&__d=dis'

import json, requests

response = requests.get(url=query_url)

if not response.status_code == 429:
    json_text = response.text
    json_data = json.loads(json_text)
</code></pre>
<ul>
  <li class=fragment data-fragment-index="1"><code class="highlight">requests</code> <sup><a href="https://docs.python-requests.org/en/latest/" class="smallsuper"><i data-customTooltip=">Built for human beings." class="fa fa-external-link"></i></a></sup></li>
  <li class=fragment data-fragment-index="1"><code class="highlight">json</code> Module <sup><a href="https://docs.python.org/3/library/json.html#module-json/" class="smallsuper"><i class="fa fa-external-link"></i></a></sup></li>
  <li class=fragment data-fragment-index="3">HTML <code class="highlight">Status Codes</code> <sup><a href="https://www.iana.org/assignments/http-status-codes/http-status-codes.xhtml" class="smallsuper"><i class="fa fa-external-link"></i></a></sup></li>
</ul>

Note:
- python can be used (requests library),
- similar to how the browser interacts with the API
- HTML error codes: 400 Bad requests, 403 Forbidden, 429 Too Many Requests, 500 Internal Server Error, 503 Service Unavailable, 504 Gateway Timeout


---

Frequent <span class="highlight">HTML status codes</span> (e.g.):

<span class="small">

| HTML Status Code | Message/Meaning       |
| ---------------- | --------------------- |
| 200              | OK                    |
| 400              | Bad request           |
| 403              | Forbidden             |
| 429              | Too Many Requests     |
| 500              | Internal Server Error |
| 503              | Service Unavailable   |
| 504              | Gateway Timeout       |

</span>

Note:
- ask: how would you react to these codes?

---

<pre class="python"><code data-trim data-noescape data-line-numbers="2|3" data-fragment-index="1">
...
json_data = json.loads(json_text)
json.dumps(json_data, indent=2)[0:550]
</code></pre>

<span class=fragment data-fragment-index="1">

json: <span class="highlight">Flexible, structured, nested</span> data format
```json
{
  "graphql": {
    "hashtag": {
      "id": "17841563668080313",
      "name": "park",
      "allow_following": false,
      "is_following": false,
      "is_top_media_only": false,
      "profile_pic_url": "https://scontent-frt3-1.cdninstagram.com/v/t51.2885-15/e15/c179.0.721.721a/s150x150/166744655_1719090121604264_9033828806473210135_n.jpg?tp=1&_nc_ht=scontent-frt3-1.cdninstagram.com&_nc_cat=108&_nc_ohc=EBg1uEzip80AX_Za9Ag&ccb=7-4&oh=5d812109d80baaa71a671236d9daaa3d&oe=608E3E1B&_nc_sid=4efc9f",
      "edge_hashtag_to_media": {
        "cou ...
```

</span>

<div class="fragment" data-fragment-index="2" style="width:95%;position:relative;bottom:10px;text-align:right;z-index:9999;">
<p style="vertical-align: bottom;" >
Open structure means not very suited for visualization.</p> 
</div>

Notes:
- common return format: json
- attribute–value pairs <code>"attribute":"value"</code>
- array (list) `[{object},{object}]`
- nesting makes it flexible, but problematic for visualization (missing standardization)

---

JSON <i class="highlight fa fa-arrow-right"></i> CSV

Notes:
- common raw interchange format for visualizations: CSV

---


<img style="height:500px;text-align:left" class="plain"  src="images/flickr-cc-by.png"/><br>


<div style="width:95%;position:absolute;bottom:100px;text-align:right;z-index:9999;">
<p style="vertical-align: bottom;" >
<strong>Flickr CC </br>100 Million </br>Dataset (CSV) </strong> <sup><a href="https://lbsn.vgiscience.org/tutorial/yfcc-introduction/" class="smallsuper"><i class="fa fa-external-link"></i></a></sup>
</p> 
</div>

Notes:
- Social Media CSV example: Flickr YFCC 100 Million dataset
- published in 2014 by Yahoo/Flickr, based on CC By images
- same data can be retrieved using the public Flickr API

---

Example: Flickr CC100M CSV

```python
from pathlib import Path

root = Path.cwd()
csv = root / 'data' / 'yfcc100m.csv'
```
<ul>
<li>Define path to CSV</li>
<li class="fragment" data-fragment-index="1"><code class="highlight">pathlib.Path</code>: OS independent filesystem paths <sup><a href="https://docs.python.org/3/library/pathlib.html#basic-use" class="smallsuper"><i class="fa fa-external-link"></i></a></sup></li>
<li class="fragment" data-fragment-index="2"><code class="highlight">Path.cwd()</code>: Current directory</li>
<li class="fragment" data-fragment-index="3">Size of dataset: <code class="highlight">~60 GB</code></li>
</ul>

Notes:
- basic code to open the CSV file
- use of pathlib!
- Is it a good idea to open a 60GB file at once?

---

Load CSV
```python
import pandas as pd

csv = "yfcc100m.csv"
df = pd.read_csv(csv, encoding='utf-8', sep='\t', header=None, nrows=5)
```

<ul>
<li class="fragment" data-fragment-index="1"><code class="highlight">Pandas</code> - Python's Excel (understatement) <sup><a href="https://pandas.pydata.org/" class="smallsuper"><i class="fa fa-external-link"></i></a></sup></li>
<li class="fragment" data-fragment-index="2"><code class="highlight">df</code>? DataFrame? 10 minutes to pandas <sup><a href="https://pandas.pydata.org/pandas-docs/stable/user_guide/10min.html" class="smallsuper"><i class="fa fa-external-link"></i></a></sup></li>
<li class="fragment" data-fragment-index="3">Docs: <code class="highlight">pd.read_csv()</code> <sup><a href="https://pandas.pydata.org/docs/reference/api/pandas.read_csv.html" class="smallsuper"><i class="fa fa-external-link"></i></a></sup></li>
<ul>
<li class="fragment" data-fragment-index="4"><code class="highlight">encoding='utf-8'</code> Character encoding</li>
<li class="fragment" data-fragment-index="5"><code class="highlight">sep='\t'</code> CSV column separator</li>
<li class="fragment" data-fragment-index="6"><code class="highlight">header=None</code> Specify wether first row contains column labels</li>
<li class="fragment" data-fragment-index="7"><code class="highlight">nrows=5</code> Fetch first 5 rows</li>
</ul>
</ul>

Notes:
- pandas to the rescue: Importance of DataFrame in current data science
- streaming of CSV file (chunked processing)
- explain basic syntax

---

<div style="height:99vh;">

Preview
```python
df.head()
```

<div style="width:100%;height:100%;position:absolute;z-index:99">
    <img class="plain fragment" data-fragment-index="1" style="position: absolute;left: 50%;top: 0%;transform: translate(-50%, 15%);" src="images/preview-ccby.png"/>
    <img class="plain fragment" data-fragment-index="2" style="position: absolute;left: 50%;top: 0%;transform: translate(-50%, 15%);" src="images/preview-ccby-dataframe1.png"/>
    <img class="plain fragment" data-fragment-index="3" style="position: absolute;left: 50%;top: 0%;transform: translate(-50%, 15%);" src="images/preview-ccby-dataframe2.png"/>
    <img class="plain fragment" data-fragment-index="4" style="position: absolute;left: 50%;top: 0%;transform: translate(-50%, 15%);" src="images/preview-ccby-dataframe3.png"/>
    <img class="plain fragment" data-fragment-index="5" style="position: absolute;left: 50%;top: 0%;transform: translate(-50%, 15%);" src="images/preview-ccby.png"/>
    <img class="plain fragment" data-fragment-index="6" style="position: absolute;left: 50%;top: 0%;transform: translate(-50%, 15%);" src="images/preview-ccby-noheader.png"/>
    <img class="plain fragment" data-fragment-index="7" style="position: absolute;left: 50%;top: 0%;transform: translate(-50%, 15%);" src="images/preview-ccby-what.png"/>
    <img class="plain fragment" data-fragment-index="8" style="position: absolute;left: 50%;top: 0%;transform: translate(-50%, 15%);" src="images/preview-ccby-time.png"/>
    <img class="plain fragment" data-fragment-index="9" style="position: absolute;left: 50%;top: 0%;transform: translate(-50%, 15%);" src="images/preview-ccby-html.png"/>
    <img class="plain fragment" data-fragment-index="10" style="position: absolute;left: 50%;top: 0%;transform: translate(-50%, 15%);" src="images/preview-ccby-nan.png"/>
</div>
</div>

Notes:
- Easy preview
- immediate observations: Missing header
- Public dataset, information difficult to find about meaning of columns
- common situation; other issues observable

---

Standardization: Attribute Mapping

Notes:  
- conversion JSON to tabular data structure makes standardization necessary
- not a complicated but time intensive task

---

<div style="height:99vh;">

<div style="width:100%;">
    <img class="plain" style="width:362px;position:absolute;top:50px;left:300px;z-index:9999;" src="images/lbsn-structure.png"/>
    <img class="plain" style="width:1200px;position:absolute;top:75px;left:100px" src="images/attribute-mapping.png"/>
    <img class="plain fragment fade-in" data-fragment-index="1" style="width:1200px;position:absolute;top:75px;left:100px" src="images/attribute-mapping-spatial.png"/>
</div>

<div style="width:95%;position:relative;top:500px;text-align:right;z-index:9999;">
<p class="highlight" style="vertical-align: bottom;" data-customTooltip="A common language independent, privacy-aware and cross-network social-media data scheme." >
<strong>lbsn.vgiscience.org</strong> <sup><a href="https://lbsn.vgiscience.org/" class="smallsuper"><i class="fa fa-external-link"></i></a></sup>
</p> 
</div>

</div>

Notes:
- Integration of data
- time intensive, low-level task
- ability to create visualizations independent of data source

---

YFCC100M: First item.
```python
df.loc[0]
```
<ul>
<li class="fragment" data-fragment-index="1"><code class="highlight">loc-Indexer</code>: Access rows and columns by label(s) or a boolean array. <sup><a href="https://pandas.pydata.org/docs/reference/api/pandas.DataFrame.loc.html" class="smallsuper"><i class="fa fa-external-link"></i></a></sup></li>
</ul>

<div class="fragment" data-fragment-index="3" style="width:100%;height:400px;">
    <img class="fragment shadow" data-fragment-index="3" style="position: absolute;left: 50%;top: 50%;transform: translate(-50%, -50%);" src="images/first-record.png"/>
    <img class="fragment shadow" data-fragment-index="4" style="position: absolute;left: 50%;top: 50%;transform: translate(-50%, -50%);" src="images/first-record-2.png"/>
    <img class="fragment shadow" data-fragment-index="5" style="position: absolute;left: 50%;top: 50%;transform: translate(-50%, -50%);" src="images/first-record-3.png"/>
</div>

<div class="fragment" data-fragment-index="5" style="width:100%;position:absolute;top:250px;right:0px;text-align:right;z-index:9999;">
<p class="highlight" style="vertical-align: bottom;">
<strong>Flickr </br>API </br>Docs</strong></br> <sup><a href="https://www.flickr.com/services/api/flickr.photos.geo.photosForLocation.html" class="smallsuper"><i class="fa fa-external-link"></i></a></sup>
</p> 
</div>

Notes:
- explain DataFrame index
- importance of loc indexer compared to other selection methods
- geospatial data peculiarities in Flickr

---

Other methods for deriving spatial information

Gazetteer example: <span class="highlight">Instagram API</span>

Notes:
- What is a Gazetteer?

---

```python
location = "1893214"
query_url = f'https://www.instagram.com/explore/locations/{location}/?__a=1&__d=dis'
```

<ul>
<li>Link to query <sup><a href="https://www.instagram.com/explore/locations/1893214/?__a=1&__d=dis" class="smallsuper"><i class="fa fa-external-link"></i></sup></a></li>
</ul>

<div class="fragment" data-fragment-index="1" style="width:100%;">

<pre><code>
{"graphql":null,"native_location_data":{"location_info":{"location_id":"1893214","facebook_places_id":"119533068103485","name":"Gro\u00dfer 
Garten","phone":"03 51-4456-600","website":"http://www.grosser-garten-dresden.de/","category":"Public 
Square / Plaza","price_range":2,"hours":{"status":"Open 24 hours","current_status":"Open 
24 hours","hours_today":"","schedule":[]},"lat":51.037697,"lng":13.762865,"location_address":"Hauptallee 
10","location_city":"Dresden, Germany","location_region":0,"location_zip":"01219","ig_business":{},"show_location_page_survey":false,"num_guides":null,"has_menu":false,"page_effect_info":{"num_effects":0,"thumbnail_url":null,"effect":null}},"ranked":{"sections":[{"layout_type":"media_grid","layout_content":{"medias":[{"media":{"taken_at":1635240067,"pk":2692911326172298695,"id":"2692911326172298695_1677335629","device_timestamp":163524001910942,"media_type":1,"code":"CVfJJVgID3H","client_cache_key":"MjY5MjkxMTMyNjE3MjI5ODY5NQ==.2","filter_type":0,"is_unified_video":false,"location":{"pk":1893214,"short_name":"Gro\u00dfer 
Garten","facebook_places_id":119533068103485,"external_source":"facebook_places","name":"Gro\u00dfer 
Garten","address":"Hauptallee 10","city":"Dresden, Germany","has_viewer_saved":null,"lng":13.762865,"lat":51.037697,"is_eligible_for_guides":true},"lat":51.037697,"lng":13.762865,"user":{"pk":1677335629,"username":"radio_dresden","full_name":"Radio 
[...]
</code></pre>

</div>

Notes:
- location endpoint
- coupling between Instagram and Facebook Places database
- open in Firefox tab and explain json structure

---

<div style="width:100%;height:600px;">
    <img class="plain" style="position: absolute;left: 50%;top: 50%;transform: translate(-50%, -50%);" src="images/first-part.png"/>
</div>

Notes:
- broad overview, more can be done
- amount of work depends on specific context and scrutiny of the analyst

---

<ol class="roman">
  <li class="highlight">Data retrieval</li>
  <li class="highlight">Data structures, formats, attribute mapping</li>
  <li class="highlight">Types of incoming spatial data 
  <li>Data handling, aggregation</li>
  <li>Visualization</li>
  <li>Packages</li>
  <li>Environment setup and tools </li>
  <li>Code conventions</li>
  <li>Tracking: git, revision control</li>
  <li>Reproducibility: docker, WSL</li>
</ol>

<div style="width:100%;position:absolute;top:200px;right:0px;text-align:right;z-index:9999;">
<p class="highlight" style="vertical-align: bottom;">
<strong>First part</strong><sup><i class="fa fa-check"></i></sup>
</p> 
</div>

Notes:
- brief break; ask questions
- Why is the Instagram API still publicly accessible, without authentication?

---

<ol class="roman">
  <li>Data retrieval</li>
  <li>Data structures, formats, attribute mapping</li>
  <li>Types of incoming spatial data 
  <li class="highlight">Data handling, aggregation</li>
  <li class="highlight">Visualization</li>
  <li>Packages</li>
  <li>Environment setup and tools </li>
  <li>Code conventions</li>
  <li>Tracking: git, revision control</li>
  <li>Reproducibility: docker, WSL</li>
</ol>
