Aggregation Example: Usercount per Country

<!-- .slide: data-background-image="images/sel_05_comp_raw.png" -->

Notes:
- introduce simple application example:
- given: users measured per 100km grid cells
- summarize user counts per country (YFCC dataset)

---

Two high-level tasks

1. Assign 100x100 km cells to countries <span class="fragment" data-fragment-index="1">(<span class="highlight">A geometric operation</span>)</span>
2. Count distinct users per country <span class="fragment" data-fragment-index="1">(<span class="highlight">A summary/aggregation operation</span>)</span>

Notes:
- complex task: divide into subtasks

---

Supplementary data <i class="highlight fragment fa fa-arrow-right" data-fragment-index="1"> Integration of information</i>

Notes:
- often, integration of data necessary, 
- based on additional, external information

---

Define origin of supplementary data in your code:
```python
DATA_PATH = Path.cwd() / "data"
FILENAME = "ne_50m_admin_0_map_subunits.shp"
```
<ul>
<li class="fragment" data-fragment-index="1"><code class="highlight">Natural Earth</code> map units shapefile (1:50m) <sup><a href="https://www.naturalearthdata.com/downloads/50m-cultural-vectors/50m-admin-0-details/" class="smallsuper"><i class="fa fa-external-link"></i></a></sup></span></li>
</ul>

Notes:
- reproducibility also means that origin of data is tracked
- e.g. update/new release

---

<div style="height:99vh;">
<br><br>

Preview data
<pre class="python"><code data-trim data-noescape data-line-numbers="1-2|3|4-6" data-fragment-index="1">
DATA_PATH = Path.cwd() / "data"
FILENAME = "ne_50m_admin_0_map_subunits.shp"
import geopandas as gp
world = gp.read_file(
    DATA_PATH / FILENAME)
world.plot()
</code></pre>

<ul>
<li class="fragment" data-fragment-index="1"><code class="highlight">Geopandas</code>: DataFrame extension for geospatial data<sup><a href="https://geopandas.org/en/stable/" class="smallsuper"><i class="fa fa-external-link"></i></a></sup></li>
<li class="fragment showmap" data-fragment-index="2"><code class="highlight">matplotlib.pyplot.plot</code><sup><a href="https://matplotlib.org/stable/api/_as_gen/matplotlib.pyplot.plot.html" class="smallsuper"><i class="fa fa-external-link"></i></a></sup></li>
</ul>


<div class="fragment" data-fragment-index="2" style="position:absolute;top:0px;left:0px;width:150%;height:110%;z-index:-1000;">
    <img class="plain" style="width:100%;margin-top:10px;left:25%;transform:translateX(-15%);" src="images/ne_countries.png"/>
</div>
</div>

Notes:
- fast preview generation of geospatial data
- with geopandas and matplotlib
- Explain geopandas as a spatial extension to pandas
- Access to all methods of pandas, spatial

---

Define <span class="highlight">full</span> origin of supplementary data in your code:
```python
DATA_PATH = Path.cwd() / "data"
URI = "https://www.naturalearthdata.com/http//www." \
      "naturalearthdata.com/download/50m/cultural/"
FILENAME = "ne_50m_admin_0_map_subunits.zip"
```

Notes:
- Improvement: define _full_ origin of data

---

This includes unzipping!

<span class="fragment" data-fragment-index="1">Unzipping? In python? Without writing to disk?</span><span class="fragment highlight" data-fragment-index="2"> <img class="plain highlight" style="width:75px;bottom:-10px" src="assets/grin-stars-solid.svg"/> Google <sup><a href="https://www.google.com/search?q=python+unzipping+a+.zip+file+without+writing+to+disk" class="smallsuper"><i class="fa fa-external-link"></i></a></sup></span>

Notes:
- ReadTheDocs or ask Google?
- A single Package: ReadTheDocs
- Broader questions (packages unknown): Google is Ok, but..

---

<iframe class="stretch" frameBorder="0" data-src="https://alexanderdunkel.com/iframe.php"></iframe>

<!-- .slide: data-background="#232323" -->

Notes:
- Python is 20 years old
- all kinds of knowledge from different versions,
- people with different skills
- makes it very difficult and problematic to copy&paste

---

Four lines of code:
<pre class="python"><code data-trim data-noescape data-line-numbers="1-4|1|2|3|4" data-fragment-index="1">
import requests, zipfile, io
r = requests.get(f'{URI}{FILENAME}')
z = zipfile.ZipFile(io.BytesIO(r.content))
z.extractall(DATA_PATH)
</code></pre>
<ul>
<li class="fragment" data-fragment-index="1"><span class="highlight">zipfile</span>, <span class="highlight">io</span>: Base packages included with Python</li>
<li class="fragment" data-fragment-index="2">Retrieve data</li>
<li class="fragment" data-fragment-index="3">Read response into ZipFile Object</li>
<li class="fragment" data-fragment-index="4">Extract to <span class="highlight">DATA_PATH</span> directory</li>
</ul>


Notes:
- explore options, but consciously write code

---

<div style="height:99vh;position:relative"><br><br><br><br><br>
<span class="fragment fade-out" data-fragment-index="1">Task 2: Counting users</span>
<br>
<div class="fragment" data-fragment-index="1" style="position:absolute;top:0;left:0;height:600px;width:100%;z-index:0">
    <img class="plain" style="width:100%;margin-top:0px;position:absolute;left:50%;transform:translateX(-50%)" src="images/grid_shapes.png"/>
</div>

<div class="highlight fragment r-fit-text" data-fragment-index="1" style="width:100%;height:100px;position:absolute;top:550px;left:50px;text-align:left;z-index:1000;background-color: rgba(255,255,255, 0.8);">
<ul style="vertical-align: bottom;">
<li>Background: Pre-aggregated grid with a set of user for each cell</li>
<li class="fragment" data-fragment-index="2">Foreground: Country shapes</li>
</ul>
</div>
</div>

Notes:
- back to application context
- illustrate starting situation

---

<pre class="python"><code data-trim data-noescape data-line-numbers="1-4|1|2-3|4-5" data-fragment-index="1">
users_set = set()
user = "96117893@N05"
users_set.add(user)
print(len(users_set))
> 1
</code></pre>
Counting distinct users: Low-level approach
<ul style="vertical-align: bottom;">
<li class="fragment" data-fragment-index="1">Define empty set</li>
<li class="fragment" data-fragment-index="2">Add first user-id to set</li>
<li class="fragment" data-fragment-index="3">Calculate the length of the set</li>
</ul>

Notes:
- common approach to divide complex tasks into simple sub-tasks
- simplest way to count distinct users in python

---

Intersection

```python
grid_overlay = gp.overlay(
    grid, world, 
    how='intersection')
```

<ul>
<li><code class="highlight">grid</code> & <code class="highlight">world</code>: GeoDataFrames<sup><a href="https://geopandas.org/en/stable/docs/reference/geodataframe.html" class="smallsuper"><i class="fa fa-external-link"></i></a></sup></span></li>
<li class="fragment" data-fragment-index="1"> GeoDataFrames support all functions of regular Pandas DataFrames (via inheritance)</li>
<li class="fragment" data-fragment-index="2"><code class="highlight">Overlay operations</code>: </br>‘intersection’, ‘union’, ‘identity’, ‘symmetric_difference’ or ‘difference’ <sup><a href="https://geopandas.org/en/stable/docs/reference/api/geopandas.GeoDataFrame.overlay.html" class="smallsuper"><i class="fa fa-external-link"></i></a></sup></span></li>
</ul>

Notes:
- common geometric operations, similar to standard GIS
- explain GeoDataFrame

---

<br>

Preview Intersection
<pre class="python"><code data-trim data-noescape data-line-numbers="1-7|2|3|4-7" data-fragment-index="1">
country_col = 'su_a3' # Column with country codes
query = grid_overlay[country_col].isin(["DEU", "FXX"])
sel = grid_overlay[query]
sel.plot(
    edgecolor='white',
    column=country_col,
    linewidth=0.3)
</code></pre>
<ul>
<li class="fragment" data-fragment-index="1"><code class="highlight">query</code>: Boolean Indexing ("list" of True/False)<sup><a href="https://pandas.pydata.org/pandas-docs/stable/user_guide/indexing.html#basics" class="smallsuper"><i class="fa fa-external-link"></i></a></sup></span></li>
<li class="fragment" data-fragment-index="2"><code class="highlight">df[query]</code>: Create a "slice" of the DataFrame</span></li>
</ul>

<div class="fragment" data-fragment-index="3" style="position:absolute;top:0;left:0;height:600px;width:100%;z-index:9999;">
    <img class="plain" style="height:100%;margin-top:0px;position:absolute;left:50%;transform:translateX(-50%);" src="images/france_de.png"/>
</div>

Notes:
- explain chaining of operations
- preview generation
- possible to split complex tasks further into separate lines

---

<div style="height:99vh">
<br><br><br><br><br>

```python
grid.plot(edgecolor='white', column=country_col, figsize=(22,28), linewidth=0.3)
```

<div class="fragment" data-fragment-index="1" style="position:absolute;top:50px;left:0;height:500px;width:100%;z-index:-1000;">
    <img class="plain" style="width:100%;margin-top:20px;position:absolute;left:50%;transform:translateX(-50%);" src="images/map_grid.png"/>
</div>
</div>

Notes:
- final result of the intersection

---

Count users:
<pre class="python" data-id="code-animation2"><code data-trim data-noescape data-line-numbers data-fragment-index="1">
usercounts_country = grid["user_guid"].groupby(country_col).size()
</code></pre>
<ul>
<li style="display: none;">Select column for counting (<code class="highlight">pd.Series</code>)<sup><a href="https://pandas.pydata.org/docs/reference/api/pandas.Series.html" class="smallsuper"><i class="fa fa-external-link"></i></a></sup></li>
<li style="display: none;">Group user ids per country column (<code class="highlight">.groupby()</code>)<sup><a href="https://pandas.pydata.org/docs/reference/api/pandas.Series.groupby.html" class="smallsuper"><i class="fa fa-external-link"></i></a></sup></li>
<li style="display: none;">Return the number of elements in the underlying data. (<code class="highlight">.size()</code>)<sup><a href="https://pandas.pydata.org/docs/reference/api/pandas.Series.size.html" class="smallsuper"><i class="fa fa-external-link"></i></a></sup></li>
</ul>

<!-- .slide: data-auto-animate -->

---

Count users:
<pre class="python" data-id="code-animation2"><code data-trim data-noescape data-line-numbers="1-3|1|2|3" data-fragment-index="1">
usercounts_country = grid["user_guid"] \
    .groupby(country_col) \
    .size()
</code></pre>
<ul>
<li class="fragment" data-fragment-index="1">Select column for counting (<code class="highlight">pd.Series</code>)<sup><a href="https://pandas.pydata.org/docs/reference/api/pandas.Series.html" class="smallsuper"><i class="fa fa-external-link"></i></a></sup></li>
<li class="fragment" data-fragment-index="2">Group user ids per country column (<code class="highlight">.groupby()</code>)<sup><a href="https://pandas.pydata.org/docs/reference/api/pandas.Series.groupby.html" class="smallsuper"><i class="fa fa-external-link"></i></a></sup></li>
<li class="fragment" data-fragment-index="3">Return the number of elements in the underlying data. (<code class="highlight">.size()</code>)<sup><a href="https://pandas.pydata.org/docs/reference/api/pandas.Series.size.html" class="smallsuper"><i class="fa fa-external-link"></i></a></sup></li>
</ul>

<!-- .slide: data-auto-animate -->

Notes:
- Remaining step: Counting/aggregating users per country
- explain pandas operations
- Duplicate counts!

---

Count <span class="highlight">distinct</span> users:
<pre class="python"><code data-trim data-noescape data-line-numbers="1-5|1|2|3|4|5" data-fragment-index="1">
series_grouped = grid["user_guid"] \
    .groupby(country_col) \
    .apply(set)
usercounts_country = series_grouped.apply(len)
world.loc[usercounts_country.index, "usercount"] = usercounts_country
</code></pre>
<ul>
<li class="fragment" data-fragment-index="1">1. Take column "user_guid" </li>
<li class="fragment" data-fragment-index="2">2. Group by "country" column </li>
<li class="fragment" data-fragment-index="3">3. Apply "set" to resulting value-groups </li>
<li class="fragment" data-fragment-index="4">4. Get length of sets (number of distinct users) </li>
<li class="fragment" data-fragment-index="5">5. Store result into new column "usercount"</li>
</ul>

Notes:
- Users may be present multiple times
- count distinct: similar operations
- recapitulate simple low-level user counting with sets

---

Visualize data

---

<div style="height:99vh;position:relative">
<br><br><br><br><br>

```python
import geoviews as gv
gv.Polygons(
    world, crs=crs.Mollweide(), vdims='usercount')
```

<div class="fragment" data-fragment-index="1" style="position:absolute;top:0;left:0;height:100%;width:100%;z-index:-1000;">
    <iframe class="stretch" frameBorder="0" style="width:100%;height:100%;margin-top:50px;position:absolute;left:50%;transform:translateX(-50%);" data-src="assets/sample1.html"></iframe>
</div>

<ul class="fragment" data-fragment-index="1">
<li class="highlight" style="background-color: rgba(255,255,255, 0.9);">Geoviews<sup><a href="https://geoviews.org/index.html" class="smallsuper"><i class="fa fa-external-link"></i></a></sup></li>
</ul>
</div>

Notes: 
- fast preview with matplotlib during development
- sharing with others: Interactive Visualizations
- Geoviews compatible with GeoDataFrames (ad-hoc)

---

<div style="height:99vh;position:relative">
<br><br><br><br><br>

<pre class="python"><code data-trim data-noescape>
gv.Polygons(world, crs=crs.Mollweide(), vdims='usercount') + \
    gv.Path(world, crs=crs.Mollweide())
</code></pre>
<br><br>
<ul class="fragment" data-fragment-index="1">
<li class="highlight">Stack or combine plots<sup><a href="https://geoviews.org/user_guide/Geometries.html#feature" class="smallsuper"><i class="fa fa-external-link"></i></a></sup></li>
</ul>

<div class="fragment" data-fragment-index="1" style="position:absolute;top:50px;left:0;height:100%;width:100%;z-index:-1000;">
    <iframe class="stretch" frameBorder="0" style="width:100%;height:100%;margin-top:50px;position:absolute;left:50%;transform:translateX(-50%);" data-src="assets/sample.html"></iframe>
</div>
</div>

Notes:
- explain basic syntax for map layout/layers in gv

---

<div style="height:99vh">
<br><br><br>

Export to HTML file
```python
import holoviews as hv
hv.save(layers, 'sample.html', backend='bokeh')
```

<div class="fragment" data-fragment-index="1" style="position:absolute;top:0;left:0;height:100%;width:100%;z-index:-1000;">
    <iframe class="stretch" frameBorder="0" style="width:100%;height:100%;margin-top:50px;position:absolute;left:50%;transform:translateX(-50%);" data-src="assets/yfcc_userdays_est.html"></iframe>
</div>

<div class="fragment highlight" data-fragment-index="1" style="width:100%;position:absolute;top:350px;left:0px;text-align:left;z-index:9999;">
<p>Workshop </br>result</p>
</div>
</div>

Notes:
- Pyviz: Visualization packages with compatible APIs
- Holoviews: high-level plotting API to Bokeh interactive maps
- example: Generate standalone archive HTML from Geoviews Layers

---

<span class="highlight">You are free to:</span>

<ul>
  <li ><span class="highlight">Export</span> data to shapefiles <span class="highlight">at any time</span></li>
  <li >& continue layouting in <span class="highlight">QGis</span> (or ArcMap etc.)</li>
  <li class="fragment" data-fragment-index="1"><span class="highlight">Integrate python gradually</span>, by replacing small parts of your workflow</li>
</ul>

Notes:
- Unlike proprietary software, interoperability is a strength
- Incremental and gradual integration of python into existing workflows

---

<ol class="roman">
  <li >Data retrieval</li>
  <li >Data structures, formats, attribute mapping</li>
  <li >Types of incoming spatial data 
  <li class="highlight">Data handling, aggregation</li>
  <li class="highlight">Visualization</li>
  <li>Packages</li>
  <li>Environment setup and tools </li>
  <li>Code conventions</li>
  <li>Tracking: git, revision control</li>
  <li>Reproducibility: docker, WSL, Jupyter</li>
</ol>

<div style="width:100%;position:absolute;top:256px;right:0px;text-align:right;z-index:9999;">
<p class="highlight" style="vertical-align: bottom;">
<strong>Second part</strong><sup><i class="fa fa-check"></i></sup>
</p> 
</div>

Notes:
- Further steps not shown here is extensibility through extensions 
- & customizations of visualization (open ended topic)
